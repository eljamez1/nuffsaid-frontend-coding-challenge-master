import React from "react";
import ListItem from './ListItem'

const List = ({
  list,
  handleClearMessage,
  priority,
}) => {
  return (
    <div>
      {list.map((message, key) => (
        <ListItem priority={priority} message={message} handleClearMessage={handleClearMessage} key={key} />
      ))}
    </div>
  )
}

export default List;