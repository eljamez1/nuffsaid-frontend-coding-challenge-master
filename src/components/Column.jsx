import React from "react";
import {
  Card,
  Grid,
  Typography,
  CardContent,
} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import { CSS_VARS } from "../utils/constants"
import List from './List'

const useStyles = makeStyles({
  column: {
    margin: CSS_VARS.spSmall,
  },
  title: {
    border: 0,
    backgroundColor: 'transparent',
    textShadow: '0 1px 2px black',
    color: 'white',
  }
});

const Column = ({
  column,
  colCount,
  list,
  handleClearMessage,
}) => {
  const classes = useStyles();
  return (
    <Grid item xs className={classes.column} >
      <Card elevation={0} className={classes.title}>
        <CardContent>
          <Typography variant="h5" component="h5" gutterBottom>
            {column.title}
          </Typography>
          <Typography variant="body1" component="p">
            Count {colCount}
          </Typography>
        </CardContent>
      </Card>
      {list.length > 0 && <List priority={column.priority} list={list} handleClearMessage={handleClearMessage} />}
    </Grid >
  )
}

export default Column;