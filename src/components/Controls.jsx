import React from "react";
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { makeStyles } from '@material-ui/core/styles';
import { CSS_VARS } from "../utils/constants"

const useStyles = makeStyles({
  buttons: {
    margin: `${CSS_VARS.spXXLarge} auto 0`,
  }
});

const Controls = ({
  handleStopStart,
  stopStartText,
  handleClear,
}) => {
  const classes = useStyles();
  return (
    <ButtonGroup className={classes.buttons} color="primary" aria-label="outlined secondary button group">
      <Button
        variant="contained"
        onClick={handleStopStart}
      >
        {stopStartText}
      </Button>
      <Button
        variant="contained"
        onClick={handleClear}
      >
        {"Clear"}
      </Button>
    </ButtonGroup>
  )
}

export default Controls;