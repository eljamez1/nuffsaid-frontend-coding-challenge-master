import React, { useState, useEffect, useMemo, useCallback } from "react";
import { Grid } from "@material-ui/core";
import Error from "./Error";
import Column from "./Column";
import Controls from "./Controls";
import { DEFAULT_COLUMNS, SPEEDS } from "../utils/constants";
import Api from "../api";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    margin: "0",
    minHeight: "100vh",
    background:
      "linear-gradient(0deg, rgba(100,100,100,1) 0%, rgba(200,200,200,1) 100%)",
  },
});

const MessageList = () => {
  const classes = useStyles();
  const [messages, setMessages] = useState([{ message: "zop" }]);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [isApiStarted, setIsApiStarted] = useState(false);

  const messageCallback = useCallback((message) => {
    setSnackbarOpen(message.priority === 1);
    setMessages((messages) => {
      return [...messages, message];
    });
  }, []);

  const api = useMemo(
    () =>
      new Api({
        messageCallback: (message) => {
          messageCallback(message);
        },
      }),
    [messageCallback]
  );

  useEffect(() => {
    api.start();
    setIsApiStarted(true);
    return () => {
      api.stop();
      setIsApiStarted(false);
    };
  }, [api]);

  const handleCloseSnackbar = () => {
    setSnackbarOpen(false);
  };

  const handleStopStart = () => {
    if (isApiStarted) {
      api.stop();
      setIsApiStarted(false);
    } else {
      api.start();
      setIsApiStarted(true);
    }
  };

  const handleClear = () => {
    setMessages([]);
  };

  const handleClearMessage = (e) => {
    const { id } = e.currentTarget;
    setTimeout(() => {
      const filteredMessages = messages.filter(
        (message) => message.message !== id
      );
      setMessages(filteredMessages);
    }, SPEEDS.FAST);
  };

  const getLatestMessage = () => {
    if (messages.length) {
      return messages[messages.length - 1];
    }
  };

  const getColumnList = (id) => {
    return (
      messages.filter((message) => message.priority === id).reverse() || []
    );
  };

  const getStopStartText = () => {
    return isApiStarted ? "Stop Messages" : "Start Messages";
  };

  return (
    <div className={classes.root}>
      <Grid container>
        {messages.length > 0 && snackbarOpen && (
          <Error
            handleCloseSnackbar={handleCloseSnackbar}
            message={getLatestMessage()}
          />
        )}
        <Controls
          handleStopStart={handleStopStart}
          stopStartText={getStopStartText()}
          handleClear={handleClear}
        />
        <Grid container>
          {DEFAULT_COLUMNS.map((column, key) => {
            return (
              <Column
                column={column}
                colCount={getColumnList(column.priority).length}
                list={getColumnList(column.priority)}
                key={key}
                handleClearMessage={handleClearMessage}
              />
            );
          })}
        </Grid>
      </Grid>
    </div>
  );
};

export default MessageList;
