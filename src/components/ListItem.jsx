import React, { useState } from "react";
import {
  Card,
  CardContent,
  IconButton,
} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import Alert from "@material-ui/lab/Alert";
import { makeStyles } from '@material-ui/core/styles';
import { ERROR_CLASS_MAP, SEVERITY_MAP, CSS_COLORS, CSS_VARS, SPEEDS } from "../utils/constants"

const repeatedStyles = {
  marginBottom: CSS_VARS.spMid,
  transition: `all ${SPEEDS.FAST}ms ease`,
}

const useStyles = makeStyles({
  error: {
    ...repeatedStyles,
    backgroundColor: CSS_COLORS.error,
  },
  warning: {
    ...repeatedStyles,
    backgroundColor: CSS_COLORS.warning,
  },
  info: {
    ...repeatedStyles,
    backgroundColor: CSS_COLORS.info,
  },
  listItemClose: {
    transform: 'scaleY(0)',
  },
})

const ListItem = ({
  handleClearMessage,
  priority,
  message,
}) => {
  const classes = useStyles();
  const [cardClass, setCardClasses] = useState(classes[ERROR_CLASS_MAP[priority]]);
  return (
    <Card className={cardClass}>
      <CardContent>
        <Alert
          severity={SEVERITY_MAP[message.priority]}
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              id={message.message}
              onClick={(e) => {
                setCardClasses(`${cardClass} ${classes.listItemClose}`)
                handleClearMessage(e);
                setTimeout(() => {
                  setCardClasses(classes[ERROR_CLASS_MAP[priority]])
                }, SPEEDS.FAST)
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }>{message.message}
        </Alert>
      </CardContent>
    </Card>
  );
};

export default ListItem;