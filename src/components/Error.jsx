import React from "react";
import Snackbar from '@material-ui/core/Snackbar';
import Alert from "@material-ui/lab/Alert";
import { SPEEDS } from '../utils/constants'

const Error = ({
  message,
  handleCloseSnackbar,
  isOpen,
}) => {
  return (
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      key={`top, center`}
      open={isOpen || message.priority === 1}
      autoHideDuration={SPEEDS.SLOW}
      onClose={handleCloseSnackbar}>
      <Alert severity="error">{message.message}</Alert>
    </Snackbar>
  );
}

export default Error;