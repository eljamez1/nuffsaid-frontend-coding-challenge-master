export const DEFAULT_COLUMNS = [
  {
    priority: 1,
    title: "Error Type 1",
    count: 0,
    class: "error",
  },
  {
    priority: 2,
    title: "Warning Type 2",
    count: 0,
    class: "warning",
  },
  {
    priority: 3,
    title: "Info Type 3",
    count: 0,
    class: "info",
  },
];

export const SPEEDS = {
  FAST: 500,
  MED: 1000,
  SLOW: 2000,
};

export const SEVERITY_MAP = {
  1: "error",
  2: "warning",
  3: "info",
};

export const CSS_VARS = {
  spSmall: "5px",
  spMid: "10px",
  spLarge: "20px",
  spXLarge: "40px",
  spXXLarge: "80px",
};

export const CSS_COLORS = {
  error: "#F56236",
  warning: "#FCE788",
  info: "#88FCA3",
};

export const ERROR_CLASS_MAP = {
  1: "error",
  2: "warning",
  3: "info",
};
